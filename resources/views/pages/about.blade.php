@extends('layouts.app', ['title' => 'About-us'])

@section('content')
    <img src="{{ asset('images/Cm-uds-13sci2401.JPG') }} " alt="Mon image" class="rounded-full shadow-md my-12 h-40">

    <h2 class="text-gray-700">
        Built with <span class="text-pink-500">&hearts;</span> by Herve Kouam !
    </h2>

    <p class="mt-5">
        <a href="{{ route('home') }}" class="text-indigo-500 hover:text-indigo-600 underline"> Revenir a la page d'accueil</a>
    </p>
@endsection

