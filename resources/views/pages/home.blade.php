@extends('layouts.app')

@section('content')
    <img src="{{ asset('images/cmr.jpg') }} " alt="images drapeau" class="mt-12 rounded shadow-md h-32">

    <h1 class="mt-5 mb-3 text-3xl sm:text-5xl font-semibold text-indigo-600">Hello from Cameroon !</h1>

    <p class="text-lg text-gray-800">It's currently {{ date('h:i a') }} .</p>
@endsection

